jQuery.noConflict();
(function ($, PLUGIN_ID) {
  'use strict';
  let config = kintone.plugin.app.getConfig(PLUGIN_ID);
  kintone.events.on('app.record.index.show', function (event) {
    let r = event.records;
    let spaceElement = kintone.app.getHeaderSpaceElement();
    let fragment = document.createDocumentFragment();
    let headingEl = document.createElement('h3');
    headingEl.classList.add('plugin-space-heading');
    headingEl.textContent = '任务进度管理插件';

    let ganttButtonElDayTop = document.createElement('button');
    ganttButtonElDayTop.innerHTML = 'Day';
    ganttButtonElDayTop.classList.add('gantt-date');
    ganttButtonElDayTop.onclick = () => {
      ganttChart.change_view_mode('Day');
    }
    let ganttButtonElWeekTop = document.createElement('button');
    ganttButtonElWeekTop.innerHTML = 'Week';
    ganttButtonElWeekTop.classList.add('gantt-date');
    ganttButtonElWeekTop.onclick = () => {
      ganttChart.change_view_mode('Week');
    }
    let ganttButtonElMonthTop = document.createElement('button');
    ganttButtonElMonthTop.innerHTML = 'Month';
    ganttButtonElMonthTop.classList.add('gantt-date');
    ganttButtonElMonthTop.onclick = () => {
      ganttChart.change_view_mode('Month');
    }
    let ganttButtonElDayBottom = ganttButtonElDayTop.cloneNode(true);
    let ganttButtonElWeekBottom = ganttButtonElWeekTop.cloneNode(true);
    let ganttButtonElMonthBottom = ganttButtonElMonthTop.cloneNode(true);
    ganttButtonElDayBottom.onclick = () => {
      ganttChart.change_view_mode('Day');
    }
    ganttButtonElWeekBottom.onclick = () => {
      ganttChart.change_view_mode('Week');
    }
    ganttButtonElMonthBottom.onclick = () => {
      ganttChart.change_view_mode('Month');
    }

    let ganttEl = document.createElement('div');
    ganttEl.id = 'gantt-target';

    fragment.appendChild(headingEl);
    fragment.appendChild(ganttButtonElDayTop);
    fragment.appendChild(ganttButtonElWeekTop);
    fragment.appendChild(ganttButtonElMonthTop);
    fragment.appendChild(ganttEl);
    fragment.appendChild(ganttButtonElDayBottom);
    fragment.appendChild(ganttButtonElWeekBottom);
    fragment.appendChild(ganttButtonElMonthBottom);
    spaceElement.appendChild(fragment);
    let tasks = makeTask(r, config);
    let sheets = document.styleSheets;
    const sheetInsertNo = 2;
    if (sheets.length > sheetInsertNo) {
      let sheet = sheets[sheetInsertNo];
      for (let i = 0; i < tasks.length; i++) {
        let thisColor = getRandomColor();
        sheet.insertRule('.grid-row.' + tasks[i].assignee + '{fill: ' + thisColor + ';}', sheet.cssRules.length);
        sheet.insertRule('.fav-color.' + tasks[i].assignee + '{background-color: ' + thisColor + ';}', sheet.cssRules.length);
      }
    }

    let uniChargeTasks = unique(tasks);
    for (let i = 0; i < uniChargeTasks.length; i++) {
      let rectColor = document.createElement('div');
      rectColor.classList.add('fav-color');
      rectColor.classList.add(uniChargeTasks[i].assignee)
      let chargeName = document.createElement('span');
      chargeName.classList.add('charge-name');
      chargeName.innerText = uniChargeTasks[i].assignee;
      let rectColor2 = rectColor.cloneNode(true);
      let chargeName2 = chargeName.cloneNode(true);
      insertAfter(chargeName2, ganttButtonElMonthTop);
      insertAfter(rectColor2, ganttButtonElMonthTop);
      insertAfter(chargeName, ganttButtonElMonthBottom);
      insertAfter(rectColor, ganttButtonElMonthBottom);
    }

    let ganttChart = new Gantt('#gantt-target', tasks, {
      on_click: function (task) {
      },
      on_date_change: function (task, start, end) {
        let formatStart = start.Format('yyyy-MM-dd');
        let formatEnd = end.Format('yyyy-MM-dd');
        updateTaskDate(kintone, task.id, formatStart, formatEnd);
      },
      on_progress_change: function (task, progress) {
        updateTaskProgress(kintone, task.id, config.progress, progress);
      },
      on_view_change: function (mode) {
        if (mode === 'Day') {
          ganttButtonElDayTop.classList.add('button-selected');
          ganttButtonElDayBottom.classList.add('button-selected');
          ganttButtonElWeekTop.classList.remove('button-selected');
          ganttButtonElWeekBottom.classList.remove('button-selected');
          ganttButtonElMonthTop.classList.remove('button-selected');
          ganttButtonElMonthBottom.classList.remove('button-selected');
        } else if (mode === 'Week') {
          ganttButtonElDayTop.classList.remove('button-selected');
          ganttButtonElDayBottom.classList.remove('button-selected');
          ganttButtonElWeekTop.classList.add('button-selected');
          ganttButtonElWeekBottom.classList.add('button-selected');
          ganttButtonElMonthTop.classList.remove('button-selected');
          ganttButtonElMonthBottom.classList.remove('button-selected');
        } else if (mode === 'Month') {
          ganttButtonElDayTop.classList.remove('button-selected');
          ganttButtonElDayBottom.classList.remove('button-selected');
          ganttButtonElWeekTop.classList.remove('button-selected');
          ganttButtonElWeekBottom.classList.remove('button-selected');
          ganttButtonElMonthTop.classList.add('button-selected');
          ganttButtonElMonthBottom.classList.add('button-selected');
        } else if (mode === 'Year') {
          ganttButtonElDayTop.classList.remove('button-selected');
          ganttButtonElDayBottom.classList.remove('button-selected');
          ganttButtonElWeekTop.classList.remove('button-selected');
          ganttButtonElWeekBottom.classList.remove('button-selected');
          ganttButtonElMonthTop.classList.remove('button-selected');
          ganttButtonElMonthBottom.classList.remove('button-selected');
        }
      },
      bar_height: 24,
      view_mode: 'Day',
      language: 'zh'
    });
  });

})(jQuery, kintone.$PLUGIN_ID);