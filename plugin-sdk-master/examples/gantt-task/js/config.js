jQuery.noConflict();

(function ($, PLUGIN_ID) {
  'use strict';

  var $form = $('.js-submit-settings');
  var $cancelButton = $('.js-cancel-button');
  var $title = $('.js-text-title');
  var $startDate = $('.js-text-start');
  var $endDate = $('.js-text-endend');
  var $progress = $('.js-text-progress');

  var config = kintone.plugin.app.getConfig(PLUGIN_ID);
  if (config.title) {
    $title.val(config.title);
  }
  if (config.startDate) {
    $startDate.val(config.startDate);
  }
  if (config.endDate) {
    $endDate.val(config.endDate);
  }
  if (config.progress) {
    $progress.val(config.progress);
  }

  $form.on('submit', function (e) {
    e.preventDefault();
    kintone.plugin.app.setConfig({
      title: $title.val(),
      startDate: $startDate.val(),
      endDate: $endDate.val(),
      progress: $progress.val()
    }, function () {
      window.location.href = '../../flow?app=' + kintone.app.getId();
    });
  });
  $cancelButton.on('click', function () {
    window.location.href = '../../' + kintone.app.getId() + '/plugin/';
  });
})(jQuery, kintone.$PLUGIN_ID);