/*
 * Slack Plug-in
 * Copyright (c) 2018 Cybozu
 *
 * Licensed under the MIT License
 */
jQuery.noConflict();
(function($, PLUGIN_ID) {
  'use strict';

  $('#submit').on('click', function() {
    var apitoken = $('#apitoken').val();
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + apitoken
    };
    kintone.plugin.app.setProxyConfig('https://slack.com/api/chat.postMessage', 'POST', headers, {});
  });

  $('#cancel').on('click', function() {
    history.back();
  });

})(jQuery, kintone.$PLUGIN_ID);
