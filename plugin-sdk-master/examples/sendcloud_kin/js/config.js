jQuery.noConflict();

(function($, PLUGIN_ID) {

    'use strict';

    $(document).ready(function() {
        var terms = {
            'ja': {
                'apiUser': 'SendCloudのapiUser',
                'apiKey': 'SendCloudのapiKey',
            },
            'en': {
                'apiUser': 'SendCloud apiUser',
                'apiKey': 'SendCloud apiKey',
            },
            'zh': {
                'apiUser': 'SendCloud的apiUser',
                'apiKey': 'SendCloud的apiKey',
            }
        };

        // To switch the display by the login user's language
        var lang = kintone.getLoginUser().language;
        var i18n = (lang in terms) ? terms[lang] : terms['en'];
        var configHtml = $('#sendcloud-plugin-config').html();
        var tmpl = $.templates(configHtml);
        $('div#sendcloud-plugin-config').html(tmpl.render({'terms': i18n}));

        var config = kintone.plugin.app.getConfig(PLUGIN_ID);
        if (config && config.apiUser && config.apiKey) {
            var apiUser = config.apiUser;
            $('#sendcloud-plugin-api-user').val(apiUser);
            var apiKey = config.apiKey;
            $('#sendcloud-plugin-api-key').val(apiKey);
        }

        // Click "Save" to enter the settings
        $('#sendCloud-plugin-submit').click(function() {
            var apiUser = $('#sendcloud-plugin-api-user').val();
            var apiKey = $('#sendcloud-plugin-api-key').val();
            if (!apiUser) {
                alert("请输入apiUser的值");
            }
            if (!apiKey) {
                alert("请输入apiKey的值");
            }

            var config = {
                'apiUser': apiUser,
                'apiKey': apiKey
             };

             kintone.plugin.app.setConfig(config);
        });

        // Click "Cancel" to return to the previous page
        $('#sendCloud-plugin-cancel').click(function() {
            if (window.confirm('Are you sure to cancel this setting?')) {
                history.back();
            } else {
                return;
            }
        });
    });
})(jQuery, kintone.$PLUGIN_ID);