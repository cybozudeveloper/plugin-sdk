import {default as swal} from 'sweetalert2';

jQuery.noConflict();

(function($, PLUGIN_ID) {
  'use strict';

  const message = {
    'en': {
      'msg_tableTitle_fileName': 'FileName',
      'msg_tableTitle_fileSize': 'Size',
      'msg_tableTitle_fileDate': 'Date',
      'msg_btn_download': 'Download',
      'msg_btn_delete': 'Delete',
      'msg_alert_delete_title': 'Do you want to delete this file?',
      'msg_alert_delete_text': 'You can not revert it!',
      'msg_alert_delete_confirm': 'Yes, delete it!',
      'msg_alert_delete_cancel': 'Cancel',
      'msg_uploading': 'File uploading!',
      'msg_upload_progress': 'Upload progress: ',
      'msg_upload_speed': '%; Speed: ',
      'msg_upload_speed_unit': 'MB/s;'
    },
    'zh': {
      'msg_tableTitle_fileName': '文件名',
      'msg_tableTitle_fileSize': '文件大小',
      'msg_tableTitle_fileDate': '文件日期',
      'msg_btn_download': '下载',
      'msg_btn_delete': '删除',
      'msg_alert_delete_title': '您想删除这个文件吗？',
      'msg_alert_delete_text': '您将无法恢复这个文件!',
      'msg_alert_delete_confirm': '是，删除它!',
      'msg_alert_delete_cancel': '取消',
      'msg_uploading': '文件上传中!',
      'msg_upload_progress': '上传进度: ',
      'msg_upload_speed': '%; 速度: ',
      'msg_upload_speed_unit': 'MB/s;'
    }
  };

  const lang = kintone.getLoginUser().language;
  const i18n = (lang in message) ? message[lang] : message['zh'];
  const attachment = new kintoneUIComponent.Attachment();

  let config;

  const validateConfig = () => {
    config = kintone.plugin.app.getConfig(PLUGIN_ID);
    return config !== null;
  };

  if (!validateConfig()) {
    return;
  }

  const cos = new COS({
    SecretId: config.secretId,
    SecretKey: config.secretKey
  });

  const getFileName = (key) => {
    const lastIndex = key.lastIndexOf('/');
    if (lastIndex !== key.length - 1) {
      return key.substr(lastIndex + 1);
    }
    return '';
  };

  const getFileSize = (size) => {
    if (size === null || size.length === 0 || size === '0') {
      return '0 Bytes';
    }

    const unitArr = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    let index = 0;
    const srcsize = Number(size);
    index = Math.floor(Math.log(srcsize) / Math.log(1024));
    let resultSize = srcsize / Math.pow(1024, index);
    if (index !== 0) {
      resultSize = resultSize.toFixed(2);
    }
    return resultSize + ' ' + unitArr[index];
  };

  const formatDate = (dateObject, normalFormat) => {
    let result = '';
    if (dateObject instanceof Date) {
      const Y = dateObject.getFullYear();
      const M = dateObject.getMonth() + 1 < 10 ? '0' + (dateObject.getMonth() + 1) : dateObject.getMonth() + 1;
      const D = dateObject.getDate() < 10 ? '0' + dateObject.getDate() : dateObject.getDate();
      const h = dateObject.getHours() < 10 ? '0' + dateObject.getHours() : dateObject.getHours();
      const m = dateObject.getMinutes() < 10 ? '0' + dateObject.getMinutes() : dateObject.getMinutes();
      const s = dateObject.getSeconds() < 10 ? '0' + dateObject.getSeconds() : dateObject.getSeconds();
      if (normalFormat) {
        result = Y + '-' + M + '-' + D + ' ' + h + ':' + m + ':' + s;
      } else {
        result = Y + M + D + h + m + s;
      }
    }

    return result;
  };

  const getNowTime = () => {
    const nowTime = new Date();
    return formatDate(nowTime, false);
  };

  const getFileDate = (date) => {
    const tmpDateTime = date.split('T')[0].replace(/-/g, '/') + ' ' + date.split('T')[1].split('.')[0];
    const dateWithTz = new Date(tmpDateTime.substring(0, tmpDateTime.length - 1));
    dateWithTz.setHours(dateWithTz.getHours() + 8);
    return formatDate(dateWithTz, true);
  };

  kintone.events.on(['app.record.index.edit.show', 'app.record.create.show'], function(event) {
    const record = event.record;
    record[config.directory]['disabled'] = true;

    return event;
  });

  kintone.events.on('app.record.detail.show', function(event) {
    const record = event.record;
    const directory = record[config.directory]['value'];
    let prefix = '';
    if (typeof directory !== 'undefined' && directory.length > 0) {
      prefix = directory + '/';
    } else {
      return event;
    }

    const div = document.createElement('div');
    const space = kintone.app.record.getSpaceElement(config.fileList);
    let fileListHtml = '<table id="fileList"><tr><th>' + i18n.msg_tableTitle_fileName +
      '</th><th>' + i18n.msg_tableTitle_fileSize +
      '</th><th>' + i18n.msg_tableTitle_fileDate + '</th><th></th></tr>';

    const getCdnUrl = (url, cdn) => {
      const tmpUrlElement = document.createElement('a');
      tmpUrlElement.href = url;
      tmpUrlElement.hostname = cdn;
      return tmpUrlElement.href;
    };

    const downloadFile = (key) => {
      cos.getObjectUrl({
        Bucket: config.bucket,
        Region: config.region,
        Key: key,
        Sign: true,
      }, function(err, data) {
        const downloadUrl = data.Url + (data.Url.indexOf('?') > -1 ? '&' : '?') + 'response-content-disposition=attachment';
        window.open(getCdnUrl(downloadUrl, config.cdn));
      });
    };

    const deleteFile = (key) => {
      cos.deleteObject({
        Bucket: config.bucket,
        Region: config.region,
        Key: key,
      }, function(err, data) {
        window.location.reload();
      });
    };

    cos.getBucket({
      Bucket: config.bucket,
      Region: config.region,
      Prefix: prefix,
      Delimiter: '/',
    }, function(err, data) {
      const files = data.Contents;
      for (let i = 0, len = files.length; i < len; i++) {
        const file = files[i];
        if (file['Key'].lastIndexOf('/') !== file['Key'].length - 1) {
          fileListHtml += '<tr><td>' + getFileName(file['Key']) +
            '</td><td>' + getFileSize(file['Size']) +
            '</td><td>' + getFileDate(file['LastModified']) +
            '</td><td><button id="downloadFile_' + i + '" class="button">' + i18n.msg_btn_download + '</button>&nbsp;&nbsp;' +
            '<button id="deleteFile_' + i + '" class="button">' + i18n.msg_btn_delete + '</button>' +
            '</td></tr>';
        }
      }
      div.innerHTML = fileListHtml;
      space.appendChild(div);
      jQuery('[id^=downloadFile_]').click(function(e) {
        const id = e.target.id;
        const key = files[id.slice(id.lastIndexOf('_') + 1)]['Key'];
        downloadFile(key);
      });
      jQuery('[id^=deleteFile_]').click(function(e) {
        const id = e.target.id;
        const key = files[id.slice(id.lastIndexOf('_') + 1)]['Key'];
        swal.fire({
          title: i18n.msg_alert_delete_title,
          text: i18n.msg_alert_delete_text,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: i18n.msg_alert_delete_confirm,
          cancelButtonText: i18n.msg_alert_delete_cancel
        }).then((result) => {
          if (result.value) {
            deleteFile(key);
          }
        });
      });
    });

    return event;
  });

  kintone.events.on(['app.record.detail.delete.submit', 'app.record.index.delete.submit'], function(event) {
    const record = event.record;
    const directory = record[config.directory]['value'];
    let prefix = '';
    if (typeof directory !== 'undefined' && directory.length > 0) {
      prefix = directory + '/';
    } else {
      return event;
    }
    const objects = [];
    cos.getBucket({
      Bucket: config.bucket,
      Region: config.region,
      Prefix: prefix,
      Delimiter: '/',
    }, function(err, data) {
      const files = data.Contents;
      for (let i = 0, len = files.length; i < len; i++) {
        const file = files[i];
        objects.push({Key: file['Key']});
      }
      cos.deleteMultipleObject({
        Bucket: config.bucket,
        Region: config.region,
        Objects: objects,
      });
    });

    return event;
  });

  kintone.events.on('app.record.create.submit', function(event) {
    const record = event.record;
    const appId = kintone.app.getId();
    const loginUser = kintone.getLoginUser();
    record[config.directory]['value'] = appId + '/' + loginUser.name + '/' + getNowTime();

    return event;
  });

  kintone.events.on('app.record.create.submit.success', function(event) {
    const record = event.record;
    const directory = record[config.directory]['value'];
    let prefix = '';
    if (typeof directory !== 'undefined' && directory.length > 0) {
      prefix = directory + '/';
    }

    return new Promise(function(resolve) {
      cos.getBucket({
        Bucket: config.bucket,
        Region: config.region,
        Prefix: prefix,
      }, function(err, data) {
        if (data.Contents.length <= 0) {
          cos.putObject({
            Bucket: config.bucket,
            Region: config.region,
            Key: prefix,
            Body: '',
          }, function() {
            return resolve(event);
          });
        } else {
          return resolve(event);
        }
      });
    });
  });

  kintone.events.on('app.record.edit.show', function(event) {
    const record = event.record;
    const directory = record[config.directory]['value'];
    record[config.directory]['disabled'] = true;
    let prefix = '';
    if (typeof directory !== 'undefined' && directory.length > 0) {
      prefix = directory + '/';
    } else {
      return event;
    }

    const uploadSpace = kintone.app.record.getSpaceElement(config.upload);
    const uploadDiv = document.createElement('div');
    uploadDiv.appendChild(attachment.render());
    uploadSpace.appendChild(uploadDiv);
    attachment.setFiles([]);

    const fileListSpace = kintone.app.record.getSpaceElement(config.fileList);
    const fileListdiv = document.createElement('div');
    let fileListHtml = '<table id="fileList"><tr><th>' + i18n.msg_tableTitle_fileName +
      '</th><th>' + i18n.msg_tableTitle_fileSize +
      '</th><th>' + i18n.msg_tableTitle_fileDate + '</th></tr>';

    cos.getBucket({
      Bucket: config.bucket,
      Region: config.region,
      Prefix: prefix,
      Delimiter: '/',
    }, function(err, data) {
      const files = data.Contents;
      for (let i = 0, len = files.length; i < len; i++) {
        const file = files[i];
        if (file['Key'].lastIndexOf('/') !== file['Key'].length - 1) {
          fileListHtml += '<tr><td>' + getFileName(file['Key']) +
            '</td><td>' + getFileSize(file['Size']) +
            '</td><td>' + getFileDate(file['LastModified']) +
            '</td></tr>';
        }
      }
      fileListdiv.innerHTML = fileListHtml;
      fileListSpace.appendChild(fileListdiv);
    });

    return event;
  });

  kintone.events.on('app.record.edit.submit', function(event) {
    const record = event.record;
    const files = attachment.getFiles();
    if (files.length <= 0) {
      return event;
    }
    const directory = record[config.directory]['value'];

    const fileList = [];
    for (let i = 0, len = files.length; i < len; i++) {
      const file = files[i];
      let key = '';
      if (typeof directory !== 'undefined' && directory.length > 0) {
        key = directory + '/' + file.name;
      } else {
        key = file.name;
      }
      fileList.push({
        Bucket: config.bucket,
        Region: config.region,
        Key: key,
        Body: file
      });
    }

    return new Promise(function(resolve) {
      swal.fire({
        html: '<div id="alertMessage">' + i18n.msg_uploading + '</div>',
        allowOutsideClick: false,
        allowEscapeKey: false,
        showConfirmButton: false,
        onOpen: () => {
          swal.showLoading();
        }
      });
      cos.uploadFiles({
        files: fileList,
        SliceSize: 1024 * 1024,
        onProgress: function(info) {
          const percent = parseInt(info.percent * 10000, 0) / 100;
          const speed = parseInt(info.speed / 1024 / 1024 * 100, 0) / 100;
          jQuery('#alertMessage').text(i18n.msg_upload_progress + percent + i18n.msg_upload_speed + speed + i18n.msg_upload_speed_unit);
        }
      }, function(err, data) {
        swal.close();
        return resolve(event);
      });
    });
  });
})(jQuery, kintone.$PLUGIN_ID);