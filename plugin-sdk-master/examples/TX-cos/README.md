# Tencent Cos for Kintone
在Kintone App中添加插件后，用户可以在App里对腾讯的对象存储（COS）进行文件上传或下载。

# 插件
build/cos.zip

# 安装
将插件安装到Kintone, 请参考帮助文档[在kintone中安装插件](https://help.cybozu.cn/k/zh/admin/system_customization/add_plugin/plugin.html)

# 添加
将插件添加到Kintone App, 请参考帮助文档[在应用中添加插件](https://help.cybozu.cn/k/zh/user/app_settings/plugin.html)

# 使用文档
[Tencent Cos for Kintone](https://cybozudev.kf5.com/hc/kb/article/1387175/)

# License
MIT License

# Copyright
Copyright(c) Cybozu, Inc.