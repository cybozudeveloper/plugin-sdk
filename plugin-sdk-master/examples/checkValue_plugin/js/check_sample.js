/*    
* checkvalue Plug-in    
* Copyright (c) 2017 Cybozu    
*    
* Licensed under the MIT License    
*/    
(function(PLUGIN_ID) {    
    'use strict'; 
       
    // 输入模式    
    var MODE_ON = '1'; // 更改后进行检查   
     
    // 用于读取设置值的函数    
    var CONFIG = kintone.plugin.app.getConfig(PLUGIN_ID);    
    // 读取设置值    
    if (!CONFIG) {    
        return false;    
    }    
    
    var CONFIG_ZIP = CONFIG.zip;    
    var CONFIG_TEL = CONFIG.tel;    
    var CONFIG_FAX = CONFIG.fax;    
    var CONFIG_MAIL = CONFIG.mail;  
      
    // 更改后进行检查:1、 未检查:0    
    var MODE_CONFIG = CONFIG.mode;  
      
    // 邮政编码输入检查    
    function zipCheck(event) {    
        // 邮政编码的定义(6位半角数字)    
        var zip_pattern = /^\d{6}$/;    
        // 从event获取记录信息    
        var rec = event.record;    
        // 错误初始化    
        rec[CONFIG_ZIP].error = null;    
        // 输入邮政编码后，确认输入值    
        var zip_value = rec[CONFIG_ZIP].value;    
        if (zip_value) {    
            if (zip_value.length > 0) {    
                // 确认是否符合定义的模式 
                if (!(zip_value.match(zip_pattern))) {    
                    // 如果不符合，邮政编码字段显示错误内容。    
                    rec[CONFIG_ZIP].error = '邮政编码请输入6位半角数字。';    
                }    
            }    
        }    
    }  
   
    // 电话号码的输入检查 
    function telCheck(event) {    
        // TEL的定义(8位或11位半角数字)    
        var tel_pattern = /^\d{8,11}$/;    
        // 从event获取记录信息    
        var rec = event.record;    
        // 错误的初始化    
        rec[CONFIG_TEL].error = null;    
        // 输入TEL时，检查输入值    
        var tel_value = rec[CONFIG_TEL].value;    
        if (tel_value) {    
            if (tel_value.length > 0) {    
                //确认是否符合定义的模式  
                if (!(tel_value.match(tel_pattern))) {    
                    // 如果不符合，对TEL报错误信息    
                    rec[CONFIG_TEL].error = '电话号码请输入8位或11位半角数字。';    
                }    
            }    
        }    
    }    
    
    // FAX的输入检查    
    function faxCheck(event) {    
        // FAX的定义(8位半角数字)    
        var fax_pattern = /^\d{8}$/;    
        // 从event获取记录信息    
        var rec = event.record;    
        // 错误初始化    
        rec[CONFIG_FAX].error = null;    
        // 输入FAX时，确认输入值    
        var fax_value = rec[CONFIG_FAX].value;    
        if (fax_value) {    
            if (fax_value.length > 0) {    
                //确认是否符合定义的模式    
                if (!(fax_value.match(fax_pattern))) {    
                    // 不符合时，对FAX报错误信息    
                    rec[CONFIG_FAX].error = 'FAX号码请输入8位半角数字。';    
                }    
            }    
        }    
    }   
     
    // 检查邮箱地址    
    function mailCheck(event) {    
        // 邮箱地址的定义 (这里是简单的定义，如果需要更加详细的定义，请更改以下值)    
        var mail_pattern = /^([a-zA-Z0-9])+([a-zA-Z0-9._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9._-]+)+$/;    
        // 从event获取记录信息    
        var rec = event.record;    
        // 错误初始化    
        rec[CONFIG_MAIL].error = null;    
        // 由输入邮件地址输入时，确认输入值    
        var mail_value = rec[CONFIG_MAIL].value;    
        if (mail_value) {    
            if (mail_value.length > 0) {    
                // 确认是否符合定义的模式    
                if (!(mail_value.match(mail_pattern))) {    
                    // 如果不符合，对邮箱地址报错    
                    rec[CONFIG_MAIL].error = '不符合邮箱地址格式，请确认输入内容。';    
                }    
            }    
        }    
    }  
      
    // 在添加或更新事件促发时(新增记录、编辑记录、列表上的编辑记录)    
    kintone.events.on(['app.record.create.submit',    
    'app.record.edit.submit',    
    'app.record.index.edit.submit'], function(event) {    
        zipCheck(event);    
        telCheck(event);    
        faxCheck(event);    
        mailCheck(event);    
        return event;    
    });    
    
    // 更改事件（邮政编码)    
    kintone.events.on(['app.record.create.change.' + CONFIG_ZIP,    
        'app.record.edit.change.' + CONFIG_ZIP,    
        'app.record.index.edit.change.' + CONFIG_ZIP    
    ], function(event) {    
        if (MODE_CONFIG === MODE_ON) {    
            zipCheck(event);    
        }    
        return event;    
    });    
    
    // 更改事件(电话号码)    
    kintone.events.on(['app.record.create.change.' + CONFIG_TEL,    
        'app.record.edit.change.' + CONFIG_TEL,    
        'app.record.index.edit.change.' + CONFIG_TEL    
    ], function(event) {    
        if (MODE_CONFIG === MODE_ON) {    
            telCheck(event);    
        }    
        return event;    
    }); 
       
    // 更改事件(FAX)    
    kintone.events.on(['app.record.create.change.' + CONFIG_FAX,    
        'app.record.edit.change.' + CONFIG_FAX,    
        'app.record.index.edit.change.' + CONFIG_FAX    
    ], function(event) {    
        if (MODE_CONFIG === MODE_ON) {    
            faxCheck(event);    
        }    
        return event;    
    }); 
       
    // 更改事件(Mail)    
    kintone.events.on(['app.record.create.change.' + CONFIG_MAIL,    
        'app.record.edit.change.' + CONFIG_MAIL,    
        'app.record.index.edit.change.' + CONFIG_MAIL    
    ], function(event) {    
        if (MODE_CONFIG === MODE_ON) {    
            mailCheck(event);    
        }    
        return event;    
    });    
})(kintone.$PLUGIN_ID);