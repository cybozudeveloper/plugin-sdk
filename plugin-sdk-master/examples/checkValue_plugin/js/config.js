/*    
* checkvalue Plug-in    
* Copyright (c) 2017 Cybozu    
*    
* Licensed under the MIT License    
*/    
jQuery.noConflict();    
 
(function($, PLUGIN_ID) {    
    'use strict';    
     
    // 插件ID的设置    
    var KEY = PLUGIN_ID;    
    var CONF = kintone.plugin.app.getConfig(KEY);    
    // 输入模式    
    var MODE_ON = '1'; // 更改后进行检查    
    var MODE_OFF = '0'; // 更改后未进行检查    
    function escapeHtml(htmlstr) {    
        return htmlstr.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')    
            .replace(/"/g, '&quot;').replace(/'/g, '&#39;');    
    }    
     
    function setDropdown() {    
        // 获取字段类型未为“单行文本框”和“多行文本框”的字段信息，代入到选框
        KintoneConfigHelper.getFields(['SINGLE_LINE_TEXT', 'NUMBER']).then(function(resp) {    
            for (var i = 0; i < resp.length; i++) {    
                var prop = resp[i];    
                var $option = $('<option>');   
              
                $option.attr('value', escapeHtml(prop.code));    
                $option.text(escapeHtml(prop.label));    
                $('#select_checkvalue_field_zip').append($option.clone());    
                $('#select_checkvalue_field_tel').append($option.clone());    
                $('#select_checkvalue_field_fax').append($option.clone());    
                $('#select_checkvalue_field_mail').append($option.clone());    
            }    
            // 设置初始值    
            $('#select_checkvalue_field_zip').val(CONF.zip);    
            $('#select_checkvalue_field_tel').val(CONF.tel);    
            $('#select_checkvalue_field_fax').val(CONF.fax);    
            $('#select_checkvalue_field_mail').val(CONF.mail);    
        }).catch(function(err) {    
            alert(err.message);    
        });    
    }    
     
    $(document).ready(function() {   
      
        // 如已有值，将值设置给字段    
        if (CONF) {    
            // 做成下来列表    
            setDropdown();    
            $('#check-plugin-change_mode').prop('checked', false);    
            // 由change事件    
            if (CONF.mode === MODE_ON) {    
                $('#check-plugin-change_mode').prop('checked', true);    
            }    
        }    
        // 按“保存”按钮时设置输入值    
        $('#check-plugin-submit').click(function() {    
            var config = [];    
            var zip = $('#select_checkvalue_field_zip').val();    
            var tel = $('#select_checkvalue_field_tel').val();    
            var fax = $('#select_checkvalue_field_fax').val();    
            var mail = $('#select_checkvalue_field_mail').val();    
            var mode = $('#check-plugin-change_mode').prop('checked');    
            // 检查必填项    
            if (zip === '' || tel === '' || fax === '' || mail === '') {    
                alert('有必填项未填写');    
                return;    
            }    
            config.zip = zip;    
            config.tel = tel;    
            config.fax = fax;    
            config.mail = mail;    
            // 检查重复    
            var uniqueConfig = [zip, tel, fax, mail];    
            var uniqueConfig2 = uniqueConfig.filter(function(value, index, self) {    
                return self.indexOf(value) === index;    
            });    
            if (Object.keys(config).length !== uniqueConfig2.length) {    
                alert('选项重复');    
                return;    
            }    
         
            config.mode = MODE_OFF;    
            if (mode) {    
                config.mode = MODE_ON;    
            }    
            kintone.plugin.app.setConfig(config);    
        });    
     
        // 按“取消”按钮时的处理    
        $('.check-plugin-cancel').click(function() {    
            history.back();    
        });    
    });    
     
})(jQuery, kintone.$PLUGIN_ID);